#pragma once
#include <vector>
#include <string>
#include <filesystem>

#include "file.h"

namespace fs = std::filesystem;

struct Doc{
	std::string name;
	std::string content;
	std::vector<std::string> vectorContent;
	std::string current_path;
	std::string absolute_path;
};

class Document : private galaxy::File{
private:
	Doc document;
	bool m_isOpen;
public:
	bool isOpen() {
		return false;
	}

	Document(): m_isOpen(false){

	}



	bool openDocument(const std::string& doc) {
		
		if (!openFile(doc, document.vectorContent, document.content)) {
			m_isOpen = false; return false;
		}
	
		document.name = doc;
		fs::path p = doc;
		document.current_path = fs::current_path().u8string();
		document.absolute_path = fs::absolute(p).u8string();

		m_isOpen = true; return true;
	}

	Doc getInfo() {
		return document;
	}

	std::string getName() {
		return document.name;
	}
	std::string getContent() {
		return document.content;
	}
	std::string getPath() {
		return document.absolute_path;
	}

};

