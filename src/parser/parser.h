#pragma once
#include <vector>
#include <string>

class Parser {
private:
	std::vector<std::string> validInstructions;

public:
	Parser() {
		validInstructions.push_back("dlr");
		validInstructions.push_back("link");
		validInstructions.push_back("accept");
		
		validInstructions.push_back("sla");
		validInstructions.push_back("sra");
		validInstructions.push_back("sll");
		validInstructions.push_back("sl");
		validInstructions.push_back("sr");
		validInstructions.push_back("move");
		validInstructions.push_back("inc");
		validInstructions.push_back("dec");

		validInstructions.push_back("jmp");
		validInstructions.push_back("jc");
		validInstructions.push_back("jnc");
		validInstructions.push_back("jz");
		validInstructions.push_back("jnz");

		validInstructions.push_back("add");
		validInstructions.push_back("sub");

		validInstructions.push_back("and");
		validInstructions.push_back("or");
		validInstructions.push_back("xor");
		validInstructions.push_back("nand");
		validInstructions.push_back("nor");
		validInstructions.push_back("nxor");

		validInstructions.push_back("rg");
		validInstructions.push_back("sm");
		validInstructions.push_back("l");
		validInstructions.push_back("mx");
		validInstructions.push_back("t");

	}




	void addInstruction(const std::string& instruction) {
		validInstructions.push_back(instruction);
	}

};