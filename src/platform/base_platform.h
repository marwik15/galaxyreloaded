#pragma once

#include <string>

namespace galaxy {

	class base_platform{
		std::string OS;
	public:
		base_platform() {

		}

		virtual std::string openFileDialog() = 0;

	};

}