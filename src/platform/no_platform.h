#pragma once

#include <string>
#include "base_platform.h"

namespace galaxy {

	class no_platform : private galaxy::base_platform {
	public:
		no_platform() {
			OS = "unknown";
		}

		std::string openFileDialog() override {
			std::cout << "no dialog";
			return "null";
		}

	private:

	};

}