#pragma once
#include <iostream>
#include <string>
#include "base_platform.h"

namespace galaxy {

	class win_platform : private galaxy::base_platform {
	public:
		win_platform() {
			OS = "win";
		}

		std::string openFileDialog() override {
			std::cout << "win dialog";
			return "null";
		}

	private:

	};

}