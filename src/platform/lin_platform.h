#pragma once

#include <string>
#include "base_platform.h"
namespace galaxy {

	class lin_platform : private galaxy::base_platform{
	public:
		lin_platform() {
			OS = "lin";
		}

		std::string openFileDialog() override {
			std::cout << "lin dialog";
			return "null";
		}

	private:

	};

}