#pragma once

#include "win_platform.h"
#include "lin_platform.h"
#include "no_platform.h"

namespace galaxy {

	class Platform
	{
	public:

		#ifdef _WIN32
		win_platform currentPlatfrom;
		#define validPlatform
		#endif

		#ifdef __linux__ 
		lin_platform currentPlatfrom;
		#define validPlatform
		#endif

		#if !defined validPlatform
		no_platform currentPlatfrom;
		#endif 

		Platform(){}

	private:

	};

}