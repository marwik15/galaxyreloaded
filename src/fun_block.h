#pragma once

#include <string>

#include <bitset>  


typedef std::string Nbit;


namespace galaxy {
	struct SM_IN {
		
		std::bitset<4> IN1;
		std::bitset<4> IN2;
		
		bool CI;
	};
	struct SM_OUT {
		std::bitset<4> s_OUT;
		bool C;
		bool Z;
	};

	class fun_block {
	public:
		fun_block() {
			
		}
		SM_OUT SM(const SM_IN& in) {
			SM_OUT out;
			out.s_OUT = (in.IN1 | in.IN2);
			
			return SM_OUT(out);
		}
		

	private:

	};

}