﻿#ifdef _WIN32
#include <Windows.h>
#endif // _WIN32

#include "document.h"
#include "fun_block.h"

#include <iostream>

#include <nana/gui.hpp>
#include <nana/gui/widgets/label.hpp>
#include <nana/gui/widgets/button.hpp>
#include <nana/gui/widgets/menubar.hpp>
#include <nana/gui/widgets/textbox.hpp>
#include <nana/gui/widgets/toolbar.hpp>
using namespace nana;

void createSplashScreen(nana::form& fm) {
    fm.size(nana::size(402, 227));
    drawing dw(fm);
    dw.clear();
    dw.draw([](paint::graphics& graph)
            {
               
                nana::paint::image img("galaxyReloadedSplashScreen.jpg");
                img.paste(nana::rectangle(img.size()), graph, nana::point());

            });
    dw.update();
}





int main() {
    #ifdef _WIN32
   // ShowWindow(GetConsoleWindow(), SW_HIDE);
    #endif // _WIN32
 
    galaxy::SM_IN in = {1001,0011,0};

    galaxy::fun_block b;
   

    galaxy::SM_OUT re = b.SM(in);;
    std::cout << re.s_OUT;

    std::vector<Document> openedDocuments;
    openedDocuments.push_back(Document());
    openedDocuments[0].openDocument("DZIEL1.GLX");

    nana::form fm;
    nana::form splashScreen{ API::make_center(300, 200), nana::appearance(false, false, true, true, true, true, false) };
    nana::place pl(fm);
    nana::menubar mn(fm);
    nana::toolbar tb(fm,true,true);
    nana::label infoBar(fm,"info place holder");
    nana::place  place_(fm);
    nana::textbox textbox_(fm);

    fm.size(nana::size(790, 550));
    fm.caption("Galaxy Reloaded!");

    createSplashScreen(splashScreen);
    infoBar.format(true);

    pl.div("<vertical <menu weight=25>>");
    pl.field("menu") << mn;
    mn.push_back("&Plik");
    mn.push_back("Edycja");
    mn.push_back("Kompilacja");
    mn.push_back("Pomoc");
    mn.at(0).append("&Nowy Ctrl+F3", [](nana::menu::item_proxy& ip) { });
    mn.at(0).append(u8"&Otwórz F3", [](nana::menu::item_proxy& ip) { });
    mn.at(0).append("Zapisz F2", [](nana::menu::item_proxy& ip) { });
    mn.at(0).append("Zapisz jako... Ctrl+F2", [](nana::menu::item_proxy& ip) { });
    mn.at(0).append_splitter();
    mn.at(0).append(u8"&Wyjście F12", [](nana::menu::item_proxy& ip) {nana::API::exit_all(); });

    mn.at(1).append("Cofnij Ctrl+Z", [](nana::menu::item_proxy& ip) {});
    mn.at(1).append_splitter();
    mn.at(1).append("Wytnij Ctrl+X", [](nana::menu::item_proxy& ip) {});
    mn.at(1).append("Kopiuj Ctrl+C", [](nana::menu::item_proxy& ip) {});
    mn.at(1).append("Wklej Ctrl+V", [](nana::menu::item_proxy& ip) {});
    mn.at(1).append(u8"Usuń Del", [](nana::menu::item_proxy& ip) {});
    mn.at(1).append_splitter();
    mn.at(1).append("Zaznacz wszystko Ctrl+A", [](nana::menu::item_proxy& ip) {});
    mn.at(1).append_splitter();
    mn.at(1).append(u8"Znajdź... Ctrl+F", [](nana::menu::item_proxy& ip) {});
    mn.at(1).append(u8"Zamień... Ctrl+H", [](nana::menu::item_proxy& ip) {});

    mn.at(2).append("Kompilacja projektu Ctrl+F9", [](nana::menu::item_proxy& ip) {});

    mn.at(3).append("Temat pomocy F1", [](nana::menu::item_proxy& ip) {});
    mn.at(3).append_splitter();
    mn.at(3).append("Informacje o programie", [](nana::menu::item_proxy& ip) 
                    {
                      nana::form splashScreen;
                      createSplashScreen(splashScreen);
                      splashScreen.show(); 
                      splashScreen.modality();
                    });
   
    pl.collocate();

    tb.append(u8"otwórz");
    tb.append("zapisz");
    tb.append("zapisz jako");
    tb.separate();
    tb.append("kopiuj");
    tb.append("wklej");
    tb.append(u8"usuń");
    tb.separate();
    tb.append("kompilacja");
    tb.separate();
    tb.append("konwerter");
    tb.separate();
    tb.append("info");
    tb.append("pomoc");
    tb.separate();
    tb.append("tool11");
    tb.append("tool12");
    tb.append("tool13");
    tb.append("tool14");
    tb.append("tool15");
    tb.append("tool16");
    tb.separate();

    for (int i = 0; i < 16;i++) {
        tb.textout(i, true);
    }
    tb.events().selected([](const arg_toolbar& arg) {
        switch (arg.button)
        {
            case 8:
            std::cout << "compile ";
            break;
           
        }
                           });
   
    place_.div("vert <weight=5% menubar> <weight=5% tools>vert<textbox><weight=5% text>");

    textbox_.enable_dropfiles(true);
    place_["tools"] << tb;
    
    place_["textbox"] << textbox_;
    place_["text"] << infoBar;
    place_.collocate();

    fm.show();
    splashScreen.show();

    textbox_.append(openedDocuments[0].getContent(),0);

    exec();
}