#pragma once
#include <vector>
#include <string>
#include <iostream>
#include <fstream>

namespace galaxy {

	class File {
	public:
		File() {

		}

		bool checkExtension(const std::string& doc) {
			//exceptions
			return true;
		}

		bool openFile(const std::string& doc, std::vector<std::string>& lines, std::string& stringLines) {
			if (!checkExtension(doc)) { return false; }

			std::string line;
			std::ifstream file(doc);

			if (file.is_open()) {
				while (getline(file, line)) {
					lines.push_back(line);
					stringLines += line;
					stringLines += "\n";
				}
				file.close();
				return true;
			}

			else {
				std::cout << "Unable to open file";
				return false;
			}
		}

		bool saveFile() {

		}
	};
}